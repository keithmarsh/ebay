FROM ubuntu:22.04
RUN apt-get update && apt-get install -y curl ca-certificates libssl-dev pkg-config 
#try without build-essential
#RUN apt-get update && apt-get install -y curl build-essential ca-certificates libssl-dev pkg-config 
RUN update-ca-certificates
COPY target/debug/ebay target/debug/
EXPOSE 8080
ARG VERIFICATION_TOKEN
ENV VERIFICATION_TOKEN=$VERIFICATION_TOKEN
ARG ENDPOINT
ENV ENDPOINT=$ENDPOINT
ARG AMQP_ADDR
ENV AMQP_ADDR=$AMQP_ADDR
ARG MONGODB_URI
ENV MONGODB_URI=$MONGODB_URI
ARG EBAY_CLIENT_ID
ENV EBAY_CLIENT_ID=$EBAY_CLIENT_ID
ARG EBAY_CERT_ID
ENV EBAY_CERT_ID=$EBAY_CERT_ID
ARG EBAY_RU_NAME
ENV EBAY_RU_NAME=$EBAY_RU_NAME
ENV RUST_LOG=info
CMD target/debug/ebay