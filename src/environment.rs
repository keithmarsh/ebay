use reqwest::{Client, RequestBuilder};
use std::env;

pub struct Environment {
    //config_identifier: String,
    web_endpoint: String,
    api_endpoint: String,
    identity_endpoint: String,
    pub scopes: String,
    pub app_id: Box<String>,
    pub cert_id: Box<String>,
    pub ru_name: Box<String>,
}

impl Environment {
    pub fn production() -> Environment {
        Environment {
            //config_identifier: "api.ebay.com".to_owned(),
            web_endpoint: "https://auth.ebay.com/oauth2/authorize".to_owned(),
            api_endpoint: "https://api.ebay.com/identity/v1/oauth2/token".to_owned(),
            identity_endpoint: "https://apiz.ebay.com/commerce/identity/v1/user".to_owned(),
            scopes: "https://api.ebay.com/oauth/api_scope \
            https://api.ebay.com/oauth/api_scope/sell.inventory.readonly \
            https://api.ebay.com/oauth/api_scope/sell.account.readonly \
            https://api.ebay.com/oauth/api_scope/sell.fulfillment.readonly \
            https://api.ebay.com/oauth/api_scope/sell.analytics.readonly \
            https://api.ebay.com/oauth/api_scope/commerce.identity.readonly \
            https://api.ebay.com/oauth/api_scope/sell.marketing.readonly"
                .to_owned(),
            app_id: Box::new(env::var("EBAY_CLIENT_ID").unwrap()),
            cert_id: Box::new(env::var("EBAY_CERT_ID").unwrap()),
            ru_name: Box::new(env::var("EBAY_RU_NAME").unwrap()),
        }
    }
    pub fn generate_user_auth_url(&self) -> String {
        let mut auth_url = self.web_endpoint.clone();
        auth_url.push_str("?client_id=");
        auth_url.push_str(&*self.app_id);
        auth_url.push_str("&response_type=code");
        auth_url.push_str("&redirect_uri=");
        auth_url.push_str(&*self.ru_name);
        auth_url.push_str("&scope=");
        auth_url.push_str(&self.scopes);
        auth_url
    }

    pub fn generate_exchange_request(&self, code: String, client: &Client) -> RequestBuilder {
        let exch_body = [
            ("grant_type", "authorization_code"),
            ("redirect_uri", &*self.ru_name),
            ("code", &code),
        ];
        client
            .post(&self.api_endpoint)
            .form(&exch_body)
            .basic_auth(*self.app_id.clone(), Some(*self.cert_id.clone()))
    }

    pub fn generate_identiy_request(&self, token: String, client: &Client) -> RequestBuilder {
        let mut bearer = "Bearer ".to_owned();
        bearer.push_str(&token);
        client
            .get(&self.identity_endpoint)
            .header("key", "value")
            .header("Authorization", bearer)
    }
}
