use async_mongodb_session::MongodbSessionStore;
use async_session::{Session, SessionStore};
use axum::{
    async_trait, debug_handler,
    extract::{rejection::TypedHeaderRejectionReason, FromRef, FromRequestParts, Json, Query, State, TypedHeader},
    http::{header::SET_COOKIE, HeaderMap},
    response::{IntoResponse, Redirect, Response},
    routing::get,
    RequestPartsExt, Router,
};
use ebay_schema::{
    challenge::{ChallengeQuery, ChallengeResponse},
    deletion::DeletionMessage,
    ebay::EbayIdentity,
    test::TestMessage,
};
use environment::Environment;
use http::{header, request::Parts, Method, StatusCode, Uri};
use lapin::{options::BasicPublishOptions, BasicProperties, Channel, Connection, ConnectionProperties};
use lazy_static::lazy_static;
use serde::{Deserialize, Serialize};
use std::env;
use std::{net::SocketAddr, time::SystemTime};
use tower_http::trace::TraceLayer;
use tracing::{error, info};

///
/// Uses:
/// https://github.com/tokio-rs/axum/blob/main/examples/oauth/src/main.rs
///

static COOKIE_NAME: &str = "SESSION";
static COOKIE_FIELD: &str = "user";

mod environment;

lazy_static! {
    static ref AMQP_ADDR: String = env::var("AMQP_ADDR").unwrap();
    static ref VERIFICATION_TOKEN: String =
        env::var("VERIFICATION_TOKEN").expect("Env var VERIFICATION_TOKEN is required");
    static ref ENDPOINT: String = env::var("ENDPOINT").expect("Env var ENDPPOINT is required");
}

#[derive(Clone)]
struct AppState {
    session_store: MongodbSessionStore,
    amqp_chan: Channel,
}

impl FromRef<AppState> for MongodbSessionStore {
    fn from_ref(state: &AppState) -> Self {
        state.session_store.clone()
    }
}

impl FromRef<AppState> for Channel {
    fn from_ref(state: &AppState) -> Self {
        state.amqp_chan.clone()
    }
}

#[derive(Debug, Serialize, Deserialize)]
struct EbayUser {
    identity: EbayIdentity,
}

#[tokio::main()]
async fn main() {
    tracing_subscriber::fmt::init();
    info!("{}", Environment::production().scopes);
    // rabbitmq
    let conn_props = ConnectionProperties::default()
        .with_executor(tokio_executor_trait::Tokio::current())
        .with_reactor(tokio_reactor_trait::Tokio);
    let amqp_conn = Connection::connect(&AMQP_ADDR, conn_props)
        .await
        .expect(&format!("The url {} should be a valid url", AMQP_ADDR.to_string()));
    let amqp_chan = amqp_conn.create_channel().await.unwrap();

    // session store
    let mongo_uri = env::var("MONGODB_URI").expect("You must set the MONGODB_URI environment var!");
    let session_store = MongodbSessionStore::new(&mongo_uri, "ebay", "sessions").await.unwrap();

    // axum
    let app_state = AppState {
        session_store,
        amqp_chan,
    };

    let app = Router::new()
        .route("/ebay", get(challenge_handler).post(deletion_handler))
        .route("/ebay/about", get(about_handler))
        .route("/ebay/login", get(login_handler))
        .route("/ebay/auth", get(auth_handler))
        .route("/ebay/logout", get(logout_handler))
        .route("/ebay/home", get(home_handler))
        .route("/ebay/index", get(index_handler))
        .route("/ebay/test", get(test_handler))
        .fallback(echo_handler)
        .layer(TraceLayer::new_for_http())
        .with_state(app_state);

    let addr = SocketAddr::from(([0, 0, 0, 0], 8080));
    info!("listening on {}", addr);
    axum::Server::bind(&addr).serve(app.into_make_service()).await.unwrap();
}

async fn login_handler() -> impl IntoResponse {
    info!("login_service");
    Redirect::temporary(&Environment::production().generate_user_auth_url())
}

#[derive(Deserialize)]
#[allow(unused)]
struct AuthParams {
    code: String,
    expires_in: usize,
}
#[derive(Deserialize, Serialize, Debug)]
struct Tokens {
    access_token: String,
    expires_in: u32,
    refresh_token: String,
    refresh_token_expires_in: u64,
    token_type: String,
}

async fn auth_handler(auth_params: Query<AuthParams>, State(app_state): State<AppState>) -> impl IntoResponse {
    info!("auth_service");
    let client = reqwest::Client::new();
    let Ok(response) = Environment::production()
        .generate_exchange_request(auth_params.code.clone(), &client)
        .send()
        .await
    else {
        return Err(StatusCode::UNAUTHORIZED);
    };
    let body = response
        .json::<Tokens>()
        .await
        .expect("response should be marshalled into Tokens");
    // Introspect the token for user info
    let Ok(response) = Environment::production().generate_identiy_request(body.access_token, &client).send().await
    else {
        error!("Couldn't get identity");
        return Err(StatusCode::SERVICE_UNAVAILABLE);
    };
    let identity = response
        .json::<EbayIdentity>()
        .await
        .expect("response should be marshalled to EbayIdentiy");
    let user = EbayUser { identity };
    //let user = EbayUser { identity: "user".to_owned() };
    // Store session and redirect to home page
    let mut session = Session::new();
    session.insert(COOKIE_FIELD, &user).unwrap();
    let cookie_val = app_state.session_store.store_session(session).await.unwrap().unwrap();
    let cookie = format!("{}={}; SameSite=Lax; Path=/", COOKIE_NAME, cookie_val);
    let mut headers = HeaderMap::new();
    headers.insert(SET_COOKIE, cookie.parse().unwrap());
    Ok((headers, Redirect::to("/ebay/home")))
}

async fn logout_handler(
    State(store): State<MongodbSessionStore>,
    TypedHeader(cookies): TypedHeader<headers::Cookie>,
) -> impl IntoResponse {
    let cookie = cookies.get(COOKIE_NAME).unwrap();
    let session = match store.load_session(cookie.to_string()).await.unwrap() {
        Some(s) => s,
        // No session active, just redirect
        None => return Redirect::to("/ebay/index"),
    };
    store.destroy_session(session).await.unwrap();
    Redirect::to("/ebay/index")
}

// Session is optional
async fn index_handler(user: Option<EbayUser>) -> impl IntoResponse {
    match user {
        Some(u) => format!(
            "Hey {}! You're logged in!\nYou may now access `/protected`.\nLog out with `/logout`.",
            u.identity.username
        ),
        None => "You're not logged in.\nVisit `/ebay/login` to do so.".to_string(),
    }
}

#[debug_handler(state=AppState)]
async fn home_handler(user: EbayUser) -> impl IntoResponse {
    format!("Hello {:?}", user /* .username*/)
}

struct AuthRedirect;

impl IntoResponse for AuthRedirect {
    fn into_response(self) -> Response {
        Redirect::temporary("/ebay/login").into_response()
    }
}

#[async_trait]
impl<State> FromRequestParts<State> for EbayUser
where
    MongodbSessionStore: FromRef<State>,
    State: Send + Sync,
{
    // If anything goes wrong or no session is found, redirect to the auth page
    type Rejection = AuthRedirect;

    async fn from_request_parts(parts: &mut Parts, state: &State) -> Result<Self, Self::Rejection> {
        let store = MongodbSessionStore::from_ref(state);

        let cookies = parts
            .extract::<TypedHeader<headers::Cookie>>()
            .await
            .map_err(|e| match *e.name() {
                header::COOKIE => match e.reason() {
                    TypedHeaderRejectionReason::Missing => AuthRedirect,
                    _ => panic!("unexpected error getting Cookie header(s): {}", e),
                },
                _ => panic!("unexpected error getting cookies: {}", e),
            })?;
        let session_cookie = cookies.get(COOKIE_NAME).ok_or(AuthRedirect)?;

        let session = store
            .load_session(session_cookie.to_string())
            .await
            .unwrap()
            .ok_or(AuthRedirect)?;

        let user = session.get::<EbayUser>(COOKIE_FIELD).ok_or(AuthRedirect)?;

        Ok(user)
    }
}

async fn challenge_handler(code: Query<ChallengeQuery>) -> Json<ChallengeResponse> {
    info!("challenge");
    let mut compound: String = String::from(&code.challenge_code);
    compound.push_str(&VERIFICATION_TOKEN);
    compound.push_str(&ENDPOINT);
    Json(ChallengeResponse {
        challenge_response: sha256::digest(compound.as_bytes()),
    })
}

async fn deletion_handler(Json(deletion): Json<DeletionMessage>) {
    info!("delete user {}", deletion.notification.data.username);
}

async fn echo_handler(method: Method, uri: Uri) -> impl IntoResponse {
    info!("unmatched {} {}", method, uri);
    (StatusCode::OK, "echo")
}

async fn about_handler() -> impl IntoResponse {
    static CI_PIPELINE_CREATED: Option<&str> = option_env!("CI_PIPELINE_CREATED_AT");
    static CI_COMMIT_BRANCH: Option<&str> = option_env!("CI_COMMIT_BRANCH");
    static CI_COMMIT_SHORT_SHA: Option<&str> = option_env!("CI_COMMIT_SHORT_SHA");
    static CI_COMMIT_MESSAGE: Option<&str> = option_env!("CI_COMMIT_MESSAGE");
    static CI_PIPELINE_ID: Option<&str> = option_env!("CI_PIPELINE_ID");
    let message = format!(
        "Built {} from branch {} commit {} pipeline {} {}",
        &CI_PIPELINE_CREATED.unwrap_or("NA"),
        &CI_COMMIT_BRANCH.unwrap_or("NA"),
        &CI_COMMIT_SHORT_SHA.unwrap_or("NA"),
        &CI_PIPELINE_ID.unwrap_or("NA"),
        &CI_COMMIT_MESSAGE.unwrap_or("NA"),
    );
    info!("about {}", &message);
    message
}

async fn test_handler(State(app_state): State<AppState>) -> StatusCode {
    let datetime: time::OffsetDateTime = SystemTime::now().into();
    let test_message = TestMessage {
        verb: "test".to_owned(),
        message: datetime.to_string(),
    };
    let json_str = serde_json::to_string(&test_message).unwrap();
    // rabbitmq
    match app_state
        .amqp_chan
        .basic_publish(
            "",
            "ebay",
            BasicPublishOptions {
                immediate: false,
                mandatory: true,
            },
            json_str.as_bytes(),
            BasicProperties::default(),
        )
        .await
    {
        Ok(confirmation) => {
            info!("confirmed received {:#?}", confirmation);
            StatusCode::OK
        }
        Err(error) => {
            tracing::error!("message failed {}", error);
            StatusCode::BAD_GATEWAY
        }
    }
}

#[test]
fn verify_digest() {
    let challenge_code = "a8628072-3d33-45ee-9004-bee86830a22d";
    let verification_token = "71745723-d031-455c-bfa5-f90d11b4f20a";
    let endpoint = "http://www.testendpoint.com/webhook";

    let mut compound = challenge_code.to_owned();
    compound.push_str(verification_token);
    compound.push_str(endpoint);

    //let digest = digest_bytes(compound.as_bytes());
    let digest = sha256::digest(compound.as_bytes());
    assert_eq!(
        compound,
        "a8628072-3d33-45ee-9004-bee86830a22d71745723-d031-455c-bfa5-f90d11b4f20ahttp://www.testendpoint.com/webhook"
    );
    assert_eq!(
        digest,
        "ca527df75caa230092d7e90484071e8f05d63068f1317973d6a3a42593734bbb"
    );
}
